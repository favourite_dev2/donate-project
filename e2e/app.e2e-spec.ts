import { DonatePage } from './app.po';

describe('donate App', () => {
  let page: DonatePage;

  beforeEach(() => {
    page = new DonatePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
