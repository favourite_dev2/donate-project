import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResearchStatisticsComponent } from './research-statistics.component';

describe('ResearchStatisticsComponent', () => {
  let component: ResearchStatisticsComponent;
  let fixture: ComponentFixture<ResearchStatisticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResearchStatisticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResearchStatisticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
