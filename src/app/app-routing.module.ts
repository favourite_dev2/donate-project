import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AboutUsComponent }   from './about-us/about-us.component';
import { NeedFoodComponent }   from './need-food/need-food.component';
import { VolunteerComponent }   from './volunteer/volunteer.component';
import { WayToGiveComponent }   from './way-to-give/way-to-give.component';
import { HungryForChangeComponent }   from './hungry-for-change/hungry-for-change.component';
import { ResearchStatisticsComponent }   from './research-statistics/research-statistics.component';
import { IndexComponent }   from './index/index.component';
import { BlogComponent }   from './blog/blog.component';
import { DonateComponent }   from './donate/donate.component';


const routes: Routes = [
  
  { path: '', component: IndexComponent },
  { path: 'aboutus', component: AboutUsComponent },
  { path: 'needfood', component: NeedFoodComponent },
  { path: 'volunteer', component: VolunteerComponent },
  { path: 'wayToGive', component: WayToGiveComponent },
  { path: 'hungry', component: HungryForChangeComponent },
  { path: 'researchStatistics', component: ResearchStatisticsComponent },
  { path: 'blog', component: BlogComponent },
  { path: 'donate', component: DonateComponent },
  
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}