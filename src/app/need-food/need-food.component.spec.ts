import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NeedFoodComponent } from './need-food.component';

describe('NeedFoodComponent', () => {
  let component: NeedFoodComponent;
  let fixture: ComponentFixture<NeedFoodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeedFoodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NeedFoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
