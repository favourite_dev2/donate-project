import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HungryForChangeComponent } from './hungry-for-change.component';

describe('HungryForChangeComponent', () => {
  let component: HungryForChangeComponent;
  let fixture: ComponentFixture<HungryForChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HungryForChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HungryForChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
