import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AboutUsComponent } from './about-us/about-us.component';
import { NeedFoodComponent } from './need-food/need-food.component';
import { VolunteerComponent } from './volunteer/volunteer.component';
import { WayToGiveComponent } from './way-to-give/way-to-give.component';
import { HungryForChangeComponent } from './hungry-for-change/hungry-for-change.component';
import { ResearchStatisticsComponent } from './research-statistics/research-statistics.component';
import { IndexComponent } from './index/index.component';
import { BlogComponent } from './blog/blog.component';
import { DonateComponent } from './donate/donate.component';


@NgModule({
  declarations: [
    AppComponent,
    AboutUsComponent,
    NeedFoodComponent,
    VolunteerComponent,
    WayToGiveComponent,
    HungryForChangeComponent,
    ResearchStatisticsComponent,
    IndexComponent,
    BlogComponent,
    DonateComponent,
 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
